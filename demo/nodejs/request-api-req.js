var request = require('request');

// Set the headers
var headers = {
    'User-Agent':       'RAExample Agent',
}

// Configure the request
var options = {
    url: 'https://www.redasset.org/metrics/trendingCountries?aKey=[aKey]',
    method: 'GET',
    headers: headers
}

// Start the request
request(options, function (error, response, body) {
    if (error) {
        console.log(error);
    }

    if (!error && response.statusCode == 200) {
        // Print out the response body
        console.log(body);
    }
})
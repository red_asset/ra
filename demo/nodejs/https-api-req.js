const https = require('https');

https.get('https://www.redasset.org/metrics/trendingCountries?aKey=[aKey]', (res) => {
    console.log('statusCode:', res.statusCode);
    console.log('headers:', res.headers);

    res.on('data', (d) => {
        process.stdout.write(d);
    });

}).on('error', (e) => {
    console.error(e);
});

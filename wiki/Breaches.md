# Navigation

[Home](https://bitbucket.org/red_asset/ra/wiki/Home) [End-Points](https://bitbucket.org/red_asset/ra/wiki/End-points)

## End-Point Navigation

[Net](https://bitbucket.org/red_asset/ra/wiki/Net) [Threats](https://bitbucket.org/red_asset/ra/wiki/Threats) [Metrics](https://bitbucket.org/red_asset/ra/wiki/Metrics) [Breaches](https://bitbucket.org/red_asset/ra/wiki/Breaches) [Emergency](https://bitbucket.org/red_asset/ra/wiki/Emergency) 



# Breaches

## The total number of items in the database that are categorized as a breach
```
GET https://www.redasset.org/breaches/total?aKey=[key]
```

## Returns the number of times an email has been seen in dumps

```
GET https://www.redasset.org/breaches/email/[email]?aKey=[key]
```

## Lookup up an account via HIBP. 

```
GET https://www.redasset.org/breaches/hibp/[email]?aKey=[key]
```

## Returns the number of times an IPv4 has been seen in dumped data
```
GET https://www.redasset.org/breaches/ipaddress/[ip]?aKey=[key]
```

## Times a Phone Number has been seen
```
GET https://www.redasset.org/breaches/phonenumber/[phone]?aKey=[key]
```
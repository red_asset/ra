# Navigation

[Home](https://bitbucket.org/red_asset/ra/wiki/Home) [End-Points](https://bitbucket.org/red_asset/ra/wiki/End-points)

## End-Point Navigation

[Net](https://bitbucket.org/red_asset/ra/wiki/Net) [Threats](https://bitbucket.org/red_asset/ra/wiki/Threats) [Metrics](https://bitbucket.org/red_asset/ra/wiki/Metrics) [Breaches](https://bitbucket.org/red_asset/ra/wiki/Breaches) [Emergency](https://bitbucket.org/red_asset/ra/wiki/Emergency) 



# Net

## Return your current WAN IPv4 Address

```
GET https://www.redasset.org/net/ipv4?aKey=[key]
```

## Return your current WAN IPv4 Geo Location

```
GET https://www.redasset.org/net/geo?aKey=[key]
```

## Return current IPv4 Geo Location
```
GET http://www.redasset.org/net/geo/[ip]?aKey=[key]
```

## Whois a domain
```
GET https://www.redasset.org/net/whois/[domain]?aKey=[key]
```
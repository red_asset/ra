# Navigation

[Home](https://bitbucket.org/red_asset/ra/wiki/Home) [End-Points](https://bitbucket.org/red_asset/ra/wiki/End-points)

## End-Point Navigation

[Net](https://bitbucket.org/red_asset/ra/wiki/Net) [Threats](https://bitbucket.org/red_asset/ra/wiki/Threats) [Metrics](https://bitbucket.org/red_asset/ra/wiki/Metrics) [Breaches](https://bitbucket.org/red_asset/ra/wiki/Breaches) [Emergency](https://bitbucket.org/red_asset/ra/wiki/Emergency) 



# Emergency

## Returns a status report on U.S.A.'s Nuclear Power Plants via RSOE 
```
GET https://www.redasset.org/emergency/npp?aKey=[key]
```

## Returns the last 10 recorded crises situations 

```
GET https://www.redasset.org/emergency/crises?aKey=[key]
```

# Navigation

[Home](https://bitbucket.org/red_asset/ra/wiki/Home) [End-Points](https://bitbucket.org/red_asset/ra/wiki/End-points)

## End-Point Navigation

[Net](https://bitbucket.org/red_asset/ra/wiki/Net) [Threats](https://bitbucket.org/red_asset/ra/wiki/Threats) [Metrics](https://bitbucket.org/red_asset/ra/wiki/Metrics) [Breaches](https://bitbucket.org/red_asset/ra/wiki/Breaches) [Emergency](https://bitbucket.org/red_asset/ra/wiki/Emergency) 



# Metrics

## Trending threats by country
```
GET https://www.redasset.org/metrics/trendingCountries?aKey=[key]
```

## Trending threats by port

```
GET https://www.redasset.org/metrics/trendingPorts?aKey=[key]
```

## Trending threats by domain type
```
GET https://www.redasset.org/metrics/trendingDomains?aKey=[key]
```

## Trending threats by file type
```
GET https://www.redasset.org/metrics/trendingFileTypes?aKey=[key]
```
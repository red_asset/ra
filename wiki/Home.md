# Navigation

[Home](https://bitbucket.org/red_asset/ra/wiki/Home) [End-Points](https://bitbucket.org/red_asset/ra/wiki/End-points)

##  End-Point Navigation

[Net](https://bitbucket.org/red_asset/ra/wiki/Net) [Threats](https://bitbucket.org/red_asset/ra/wiki/Threats) [Metrics](https://bitbucket.org/red_asset/ra/wiki/Metrics) [Breaches](https://bitbucket.org/red_asset/ra/wiki/Breaches) [Emergency](https://bitbucket.org/red_asset/ra/wiki/Emergency) 


# Welcome

Welcome to the Red Asset API Wiki. You will be able to find API end-points, example GET requests, example data and even some code here on the wiki. Enjoy!


## Red Asset Features

* Threat Intel API
* Free User Accounts (API Key) - Rate limited
* Create a Custom Dashboard
* On-site Widget IDE (CSS, HTML, JS)


## Rate Limiting

Accounts are currently restricted to 12 requests per minute to the RA API.


## API Response Codes


```
{
  "code": 0, 
  "response": "key disabled"
}
```


```
{
  "code": 1, 
  "response": "invalid key"
}
```


```
{
  "code": 2,
  "response": "no key supplied"
}
```


```
{
  "code": 3, 
  "response": "You have made too many requests. Please wait a moment."
}
```

```
{
  "code": 4, 
  "response": "end-point disabled"
}
```

```
{
  "code": 5, 
  "response": "error"
}
```

